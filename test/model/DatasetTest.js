/*
 * Test script for model/Dataset.js
 */

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
global.XMLHttpRequest.DONE = 4;

const assert = require('assert');
const Dataset = require('../../src/model/Dataset');
const Feature = require('../../src/model/Feature');

class Datatest1 extends Dataset {
	constructor() {
		super();
		this.features = [
			new Feature(1, [ 48.12771, -1.6832758 ]),
			new Feature(2, [ 48.12771, -1.6832758 ]),
			new Feature(3, [ 48.12771, -1.6832758 ])
		];
		
		this.features.forEach(f => {
			f.pictures = [ { pictureUrl: "" } ];
		});
	}
}

describe("Model > Dataset", () => {
	describe("Constructor", () => {
		it("can't be instantiated directly", () => {
			assert.throws(() => {
				const d1 = new Dataset();
			}, TypeError);
		});
		
		it("works for subclass", () => {
			const d1 = new Datatest1();
			assert.ok(Array.isArray(d1.features));
			assert.equal(d1.features.length, 3);
		});
	});
	
	describe("getAllFeatures", () => {
		it("works for subclass", done => {
			const d1 = new Datatest1();
			d1.getAllFeatures()
			.then(res => {
				assert.equal(res[0].id, 1);
				assert.equal(res[1].id, 2);
				assert.equal(res[2].id, 3);
				done();
			})
			.catch(e => {
				assert.fail(e);
				done();
			});
		});
	});
});
