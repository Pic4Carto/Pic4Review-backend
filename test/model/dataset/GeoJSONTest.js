/*
 * Test script for model/dataset/GeoJSON.js
 */

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
global.XMLHttpRequest.DONE = 4;

const assert = require('assert');
const GeoJSON = require('../../../src/model/dataset/GeoJSON');

describe("Model > Dataset > GeoJSON", () => {
	describe("Constructor", () => {
		it("works with string geojson", () => {
			const g1 = new GeoJSON('{"type":"FeatureCollection","features":[{"type":"Feature","properties":{"k1":"v1"},"geometry":{"type":"Point","coordinates":[-2,48]}}]}');
			assert.ok(g1.id.length > 0);
		});
		
		it("works with object geojson", () => {
			const g1 = new GeoJSON({"type":"FeatureCollection","features":[{"type":"Feature","properties":{"k1":"v1"},"geometry":{"type":"Point","coordinates":[-2,48]}}]});
			assert.ok(g1.id.length > 0);
		});
		
		it("fails if no geojson given", () => {
			assert.throws(() => {
				const o1 = new GeoJSON();
			}, TypeError);
		});
		
		it("fails if geojson is invalid", () => {
			assert.throws(() => {
				const o1 = new GeoJSON('{"type":"FeatureCollection","features":[{"type":"Feature","properties":{"k1":"v1"},"geometry":{"type":"Point","coordinates":[-2,48]');
			}, SyntaxError);
		});
		
		it("fails if geojson has no feature", () => {
			assert.throws(() => {
				const o1 = new GeoJSON({"type":"FeatureCollection","features":[]});
			}, TypeError);
		});
		
		it("supports geojson having features not being points", () => {
			const g1 = new GeoJSON({"type":"FeatureCollection","features":[{"type":"Feature","properties":{"k1":"v1"},"geometry":{"type":"LineString","coordinates":[[-2,48],[-1,47]] }}]});
			assert.ok(g1.id.length > 0);
		});
	});
	
	describe("getAllFeatures", () => {
		it("works", done => {
			const g1 = new GeoJSON({"type":"FeatureCollection","features":[{"type":"Feature","properties":{"k1":"v1"},"geometry":{"type":"Point","coordinates":[-2,48]}}]});
			g1.getAllFeatures()
			.then(fts => {
				assert.equal(fts.length, 1);
				assert.ok(fts[0] !== null);
				done();
			})
			.catch(e => {
				assert.fail(e);
				done();
			});
		});
	});
});
