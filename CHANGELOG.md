# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Next version (to be released)

### Fixed
- Imagery JSON now correctly skips invalid entries
- Rework list of ignored properties from Osmose API
- Performance issues on database with deleted missions
- Clean-up Osmose name tag (title case)
- Change Osmose API URL to v0.3
- Change Mapillary API to v4


## 0.5.11 - 2019-12-07

### Fixed
- Integrated changes of Osmose API (0.3beta)


## 0.5.10 - 2019-10-14

### Added
- Camera opening angle can be set in `config.json`

### Changed
- Optimization of features and pictures updates (grouped SQL calls)
- Inactive missions disabled after 15 days (30 if people contributed to it)


## 0.5.9 - 2019-06-04

### Changed
- When a feature to review is a non-closed way, we filter out pictures being next to start or end point (to avoid "wrong street effect")
- Clearer documentation about configuration file `config.json`
- Mission can only have up to 10000 features now

### Removed
- Flickr pictures are not used anymore


## 0.5.8 - 2019-05-08

### Added
- Missions list route can retrieve missions an user contributed to

### Changed
- Using main Osmose instance for getting issues, as limit restriction is now higher


## 0.5.7 - 2019-04-27

### Added
- New missions based on Osmose or automatic detections have a thumbnail associated
- Missions list route accepts a new parameter for sorting results
- Allow definitive deletion of a mission
- Route for whole instance statistics (for UI home page)

### Changed
- Feature list routes can take user info as parameter, and returns features with skipped ones


## 0.5.6 - 2019-04-01

### Added
- Support for importing feature missions
- Importer missions fetches and closes Osmose errors
- Support for using automatically detected map features, available against pictures providers (using Pic4Carto.js)

### Changed
- Available imagery list is better filtered and sorted before being sent to front-end
- Clearer handling of ID from data providers in database

### Fixed
- Sync of Osmose features was broken due to wrong ID comparison, now Osmose missions will be updated correctly


## 0.5.4 - 2019-02-09

### Added
- Inactive missions (missions with contribution older than 3 months or missions without contributions created more than 2 months ago) are automatically disabled

### Changed
- Mission details route sends also author information


## 0.5.3 - 2018-10-28

### Added
- Statistics about pictures provider (in users route) and associated cached system created in database


## 0.5.0-1 2018-09-24

### Fixed
- Fix for update from 0.4.12 in `init.sql` and documentation


## 0.5.0 - 2018-09-24

### Added
- Database can now store complex geometries (other than points) for features.
- Next feature route can take userid as parameter, allowing to filter features being skipped by this user.
- Mission statistics route also give evolution of solved features amount.

### Changed
- Datasets handling complex geometries now saves them in database.
- Next feature route returns both centroid and complete geometry.
- Failsafe retrieval of features from different datasets.
- Pictures retrieval takes into account complex geometries (creates a buffer around them).
- Pictures are now stored directly in `feature` table, so `picture` table ans related functions are not available anymore.

### Removed
- `delayBetweenPicturesCleaning` parameter is not needed anymore


## 0.4.12 - 2018-08-24

### Added
- Mission can be marked as template.

### Changed
- Back-end version number will be synced with Pic4Review front-end. Front-end should always be running on same back-end version to ensure proper running.
- Caching for mission lists, giving better performance for all related routes.
- Caching for feature-picture associations, giving better performance for related routes.
