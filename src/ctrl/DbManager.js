const { Pool } = require('pg');
const fs = require('fs');
const path = require('path');

/**
 * DbManager allows to perform several actions over a database.
 * Particularly, you can log user actions, and retrieve profiles.
 *
 * @param {string} host DB host
 * @param {string} user DB user
 * @param {string} pass DB password
 * @param {string} database DB to use
 * @param {string} [port] DB port
 */
class DbManager {
	constructor(host, user, pass, database, port) {
		//Check parameters
		if(!host || host.length === 0) {
			throw new TypeError("Invalid hostname");
		}
		else if(!user || user.length === 0) {
			throw new TypeError("Invalid username");
		}
		else if(!database || database.length === 0) {
			throw new TypeError("Invalid database name");
		}
		else {
			port = port || 5432;
			this.dbName = database;

			this.pool = new Pool({
				user: user,
				host: host,
				database: database,
				password: pass,
				port: port
			});
		}
	}

	/**
	 * Connects to database and create structure if first-time run.
	 * @return {Promise} Promise resolves when DB is ready
	 */
	initDb() {
// 		const initSql = fs.readFileSync(path.resolve(__dirname, '../init.sql')).toString();

		return this.pool
			.query("SELECT 1+1")
			.then(res => {
				console.log("[DbManager]", "Init successful");
			});
	}

	/**
	 * Logs an HTTP query.
	 * It must be used as an Express middleware.
	 */
	logHttpQuery(req, res, next) {
		const query = 'INSERT INTO http_queries(userid, ip, path, query) VALUES ($1, $2, $3, $4)';

		const values = [ req.query.user, req.ip, req.path, JSON.stringify(req.query) ];

		this.pool
			.query(query, values)
			.catch(err => console.error(err));

		next();
	}

	/**
	 * Performs a query against DB.
	 * @param {string} sql The SQL query
	 * @param {Object[]} values The SQL parameters, use $1 .. $n to identify them in query
	 * @return {Promise} A promise solving on result rows
	 */
	query(sql, values) {
		return this.pool
		.query(sql, values);
	}

	/**
	 * Closes all connections to database.
	 */
	stop() {
		this.pool.end();
	}
}

module.exports = DbManager;
