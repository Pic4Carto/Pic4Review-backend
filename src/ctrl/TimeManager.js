const CONFIG = require("../../config.json");
const fs = require("fs");
const MissionsHandler = require("./MissionsRouteHandler");
const PicturesStore = require("./PicturesStore");

/**
 * TimeManager is a class which can regularly call services.
 */
class TimeManager {
	constructor() {
		this.timers = {};
		this.picStore = new PicturesStore();
	}

	/**
	 * Runs the service which check for outdated missions.
	 */
	startMissionUpdateService() {
		const delay = CONFIG.timers.delayBetweenMissionUpdate*1000;

		db.query("SELECT id FROM mission_outdated LIMIT 1")
		.then(d => {
			if(d.rows.length === 1) {
				const mid = d.rows[0].id;
				console.log("[TimeManager]", "Updating mission", mid);

				MissionsHandler.updateFeatures(mid)
				.then(() => {
					const res = this.picStore.preparePicturesForMission(mid, true);

					res.ready
					.then(() => {
						console.log("[TimeManager]", "Update done");
						setTimeout(() => this.startMissionUpdateService(), delay);
					})
					.catch(e => {
						console.error(e);
						setTimeout(() => this.startMissionUpdateService(), delay);
					});
				})
				.catch(e => {
					console.error(e);
					setTimeout(() => this.startMissionUpdateService(), delay);
				});
			}
			else {
				console.log("[TimeManager]", "No mission to update");
				setTimeout(() => this.startMissionUpdateService(), delay);
			}
		})
		.catch(e => {
			console.error(e);
			setTimeout(() => this.startMissionUpdateService(), delay);
		});
	}

	/**
	 * Runs the service for regular mission_rank table update
	 */
	startMissionRankUpdateService() {
		const delay = 10*60*1000;

		db.query("SELECT updateMissionRankCache()")
		.then(d => {
			console.log("[TimeManager]", "Mission rank updated");
			setTimeout(() => this.startMissionRankUpdateService(), delay);
		})
		.catch(e => {
			console.error(e);
			setTimeout(() => this.startMissionRankUpdateService(), delay);
		});
	}

	/**
	 * Runs the service for regular picture_provider_cache table update
	 */
	startPictureProviderUpdateService() {
		const delay = 6*60*60*1000; //Every 6h

		db.query("SELECT updatePictureProviderCache()")
		.then(d => {
			console.log("[TimeManager]", "Picture provider updated");
			setTimeout(() => this.startPictureProviderUpdateService(), delay);
		})
		.catch(e => {
			console.error(e);
			setTimeout(() => this.startPictureProviderUpdateService(), delay);
		});
	}

	/**
	 * Runs the service for disabling inactive missions
	 */
	startInactiveMissionDisablingService() {
		const delay = 24*60*60*1000; // Every day

		db.query("UPDATE mission SET status='canceled' WHERE id IN (SELECT id FROM mission_to_disable)")
		.then(d => {
			console.log("[TimeManager]", "Inactive missions disabled");

			db.query("UPDATE mission SET status='deleted' WHERE id IN (SELECT id FROM mission_to_delete)")
			.then(d => {
				console.log("[TimeManager]", "Long-ago canceled missions deleted");

				db.query("DELETE FROM feature f USING mission m WHERE f.mission = m.id AND m.status = 'deleted'")
				.then(d => {
					console.log("[TimeManager]", "Deleted useless features");
					setTimeout(() => this.startInactiveMissionDisablingService(), delay);
				})
				.catch(e => {
					console.error(e);
					setTimeout(() => this.startInactiveMissionDisablingService(), delay);
				});
			})
			.catch(e => {
				console.error(e);
				setTimeout(() => this.startInactiveMissionDisablingService(), delay);
			});
		})
		.catch(e => {
			console.error(e);
			setTimeout(() => this.startInactiveMissionDisablingService(), delay);
		});
	}
}

module.exports = TimeManager;
