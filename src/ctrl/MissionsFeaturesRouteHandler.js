const Osmose = require("../model/dataset/Osmose");
const P4C = require("pic4carto");
const xmlshim = require('xmlshim');

/**
 * Route handler for features-related routes.
 * @name MissionsFeaturesRouteHandler
 */

/**
 * Handle /missions/:mid/features requests.
 */
exports.list = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.params) {
		res.status(400).send({ "error": "Missing parameters" });
	}
	else if(!req.params.mid || isNaN(parseInt(req.params.mid))) {
		res.status(400).send({ "error": "Invalid mission ID" });
	}
	else {
		const params = [ parseInt(req.params.mid) ];
		const uid = req.query && req.query.userid ? req.query.userid : null;
		if(uid) { params.push(uid); }

		const sql = uid ?
			"SELECT f.id, CASE WHEN f.status = 'new' AND l.feature IS NOT NULL THEN 'skipped' ELSE f.status END AS status, ST_AsGeoJSON(geom) AS geom "
			+"FROM feature f "
			+"LEFT JOIN ("
				+"SELECT DISTINCT ON (feature) feature FROM feature_skip WHERE mission = $1 AND userid = $2 ORDER BY feature, moment DESC"
			+") l ON l.feature = f.id "
			+"WHERE f.mission = $1"
			: "SELECT id, status, ST_AsGeoJSON(geom) AS geom FROM feature WHERE mission = $1";

		db.query(sql, params)
		.then(d => {
			if(d.rows.length > 0) {
				res.status(200).send({ features: d.rows.map(o => { o.geom = JSON.parse(o.geom); return o; }) });
			}
			else {
				res.status(404).send({ "error": "No mission with given ID", "details_for_humans": "This mission has no features associated ! If you created this mission, please try to re-create it, as an error might happened" });
			}
		})
		.catch(e => {
			console.error(e);
			res.status(500).send({ "error": "Failed to retrieve mission", "details_for_humans": "Can't find this mission, are you sure that it's still online ?" });
		});
	}
};

/**
 * Handle /missions/:mid/features/next requests.
 */
exports.next = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.params) {
		res.status(400).send({ "error": "Missing parameters" });
	}
	else if(!req.params.mid || isNaN(parseInt(req.params.mid))) {
		res.status(400).send({ "error": "Invalid mission ID" });
	}
	else if(req.query && req.query.lat && isNaN(parseFloat(req.query.lat))) {
		res.status(400).send({ "error": "Invalid latitude" });
	}
	else if(req.query && req.query.lng && isNaN(parseFloat(req.query.lng))) {
		res.status(400).send({ "error": "Invalid longitude" });
	}
	else {
		const mid = parseInt(req.params.mid);
		const params = [ mid ];
		let query = "SELECT f.id, f.properties, f.status, f.pictures, ST_AsGeoJSON(f.geom) AS geom, ST_AsGeoJSON(f.geomfull) AS geomfull, f.sourceid "
					+"FROM feature f ";

		if(req.query.userid) {
			query += `LEFT JOIN feature_skip s ON f.id = s.feature AND s.userid = $${params.length+1} `;
			params.push(req.query.userid);
		}

		query += "WHERE f.mission = $1 AND f.status='new' AND f.pictures IS NOT NULL ";

		if(req.query.userid) {
			query += "AND s.id IS NULL ";
		}

		if(req.query.lng && req.query.lat) {
			query += `ORDER BY f.geom <-> ST_SetSRID(ST_Point($${params.length+1}, $${params.length+2}), 4326) `;
			params.push(req.query.lng);
			params.push(req.query.lat);
		}
		else {
			query += "ORDER BY random() ";
		}

		query += "LIMIT 1";

		db.query(query, params)
		.then(d => {
			if(d.rows.length === 1) {
				const result = d.rows[0];
				result.geom = JSON.parse(result.geom);

				// Check if further info is necessary
				db.query("SELECT datatype, (dataoptions->>'editors')::json AS editors, dataoptions->>'item' AS item FROM mission WHERE id = $1", [ req.params.mid ])
				.then(d2 => {
					if(d2.rows.length === 1) {
						const missionInfo = d2.rows[0];

						// Osmose import mission -> needs tags to apply on feature
						if(
							missionInfo.datatype === "osmose"
							&& missionInfo.editors
							&& missionInfo.editors.type === "importer"
							&& result.sourceid
						) {
							// Fetch info from Osmose
							Osmose
								.getErrorTags(result.sourceid)
								.then(tags => {
									result.properties = Object.assign(result.properties, tags);
									res.status(200).send({ feature: result });
								})
								.catch(e => {
										console.error(e);
										res.status(500).send({ "error": "Failed to retrieve Osmose tags", "details_for_humans": "Oops ! Osmose API, which is used for getting the feature to review, seems unavailable. Please retry later." });
								});
						}
						else {
							res.status(200).send({ feature: result });
						}
					}
					else {
						throw new Error("Mission not found");
					}
				})
				.catch(e => {
					console.error(e);
					res.status(500).send({ "error": "Failed to retrieve mission", "details_for_humans": "Can't find this mission, is it still online ?" });
				});
			}
			else {
				res.status(200).send({ "info": "No more new feature", "details_for_humans": "It seems that all features are already reviewed, but you can check out another mission." });
			}
		})
		.catch(e => {
			console.error(e);
			res.status(500).send({ "error": "Failed to retrieve mission feature", "details_for_humans": "Can't find any feature for this mission, is it still online ?" });
		});
	}
};

/**
 * Handle /missions/:mid/features/:fid requests.
 */
exports.update = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.params) {
		res.status(400).send({ "error": "Missing parameters" });
	}
	else if(!req.params.mid || isNaN(parseInt(req.params.mid))) {
		res.status(400).send({ "error": "Invalid mission ID" });
	}
	else if(!req.params.fid || isNaN(parseInt(req.params.fid))) {
		res.status(400).send({ "error": "Invalid feature ID" });
	}
	else if(!req.query || !req.query.username || !req.query.userid) {
		res.status(400).send({ "error": "Invalid user info" });
	}
	else if(!req.query.status || ["reviewed", "cantsee", "skipped"].indexOf(req.query.status) < 0) {
		res.status(400).send({ "error": "Invalid new feature status" });
	}
	else {
		const mid = parseInt(req.params.mid);
		const fid = parseInt(req.params.fid);

		db.query("SELECT status, properties, sourceid FROM feature WHERE id = $1 AND mission = $2", [ fid, mid ])
		.then(d => {
			if(d.rows.length === 1) {
				const sourceId = d.rows[0].sourceid;
				const prevStatus = d.rows[0].status;
				const props = d.rows[0].properties;

				//Change feature status
				if(req.query.status === "reviewed" || req.query.status === "cantsee") {
					db.query("UPDATE feature SET status = $1, lastedit = current_timestamp WHERE id = $2 AND mission = $3", [ req.query.status, fid, mid ])
					.then(() => {
						//Create user edit
						db.query(
							"INSERT INTO edit(userid, username, prevstatus, newstatus, mission, feature) VALUES($1, $2, $3, $4, $5, $6)",
								[ req.query.userid, req.query.username, prevStatus, req.query.status, mid, fid ]
						)
						.then(() => {
							res.status(200).send({ "info": "Update successful" });
						})
						.catch(e => {
							console.error(e);
							res.status(500).send({ "error": "Failed to save edit", "details_for_humans": "I'm sorry, I saved your edit but can't count it in your statistics due to an error..." });
						});
					})
					.catch(e => {
						console.error(e);
						res.status(500).send({ "error": "Failed to update feature", "details_for_humans": "I wasn't able to save your edit, can you retry a bit later ?" });
					});

					// Parallel processing : update feature elsewhere
					if(req.query.status === "reviewed" && sourceId) {
						// Check if further info is necessary
						db.query("SELECT datatype, (dataoptions->>'editors')::json AS editors FROM mission WHERE id = $1", [ mid ])
						.then(d2 => {
							if(d2.rows.length === 1) {
								const missionInfo = d2.rows[0];

								// Osmose import mission -> needs tags to apply on feature
								if(
									missionInfo.datatype === "osmose"
									&& missionInfo.editors
									&& missionInfo.editors.type === "importer"
								) {
									// Fetch info from Osmose
									Osmose
										.setErrorFixed(sourceId)
										.catch(e => {
											console.error(e);
										});
								}
							}
						})
						.catch(e => {
							console.error(e);
						});
					}
				}
				else if(req.query.status === "skipped") {
					db.query(
						"INSERT INTO feature_skip(userid, username, mission, feature) VALUES($1, $2, $3, $4)",
						[ req.query.userid, req.query.username, mid, fid ]
					)
					.then(() => {
						res.status(200).send({ "info": "Update successful" });
					})
					.catch(e => {
						console.error(e);
						res.status(500).send({ "error": "Failed to update feature", "details_for_humans": "I wasn't able to save your edit, can you retry a bit later ?" });
					});
				}
			}
			else {
				res.status(400).send({ "error": "Can't find given feature", "details_for_humans": "I can't find this feature, are you sure the mission is still online ?" });
			}
		})
		.catch(e => {
			console.error(e);
			res.status(500).send({ "error": "Failed to retrieve given feature", "details_for_humans": "Something went wrong when looking for this feature, can you retry a bit later ?" });
		});
	}
};


/**
 * Handle /missions/:mid/export/missing calls.
 */
exports.exportMissing = function(req, res) {
	const validFormats = [ "gpx", "geojson", "kml" ];

	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.params || !req.query) {
		res.status(400).send({ "error": "Missing parameters" });
	}
	else if(!req.params.mid || isNaN(parseInt(req.params.mid))) {
		res.status(400).send({ "error": "Invalid mission ID" });
	}
	else if(!req.query.format || validFormats.indexOf(req.query.format) < 0) {
		res.status(400).send({ "error": "Invalid export format" });
	}
	else {
		const mid = parseInt(req.params.mid);

		db.query("SELECT ST_X(f.geom) AS lon, ST_Y(f.geom) AS lat, COALESCE(f.properties->>'name', f.properties->>'title', f.properties->>'id', 'Feature') AS label FROM feature f WHERE f.mission = $1 AND f.status!='reviewed' AND (f.pictures IS NULL OR json_array_length(f.pictures) = 0)", [ mid ])
		.then(d => {
			if(d.rows.length > 0) {
				let result = null;

				switch(req.query.format) {
					case "geojson":
						const g = { type: "FeatureCollection", features: d.rows.map(l => {
							return { type: "Feature", geometry: { type: "Point", coordinates: [ l.lon, l.lat ] }, properties: { name: l.label } };
						}) };
						result = JSON.stringify(g);
						break;

					case "gpx":
						const gpxns1 = "http://www.topografix.com/GPX/1/1";
						const gpxns2 = "http://www.w3.org/2000/xmlns/";
						const gpxns3 = "http://www.w3.org/2001/XMLSchema-instance";

						const gpxdoc = xmlshim.implementation.createDocument(gpxns1, "", null);
						const gpx = gpxdoc.createElementNS(gpxns1, "gpx");
						gpx.setAttributeNS(gpxns2, "xmlns:xsi", gpxns3);
						gpx.setAttributeNS(gpxns3, "xsi:schemaLocation", "http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd");
						gpx.setAttribute("version", "1.1");
						gpx.setAttribute("creator", "Pic4Review");

						d.rows.forEach(l => {
							const entry = gpxdoc.createElementNS(null, "wpt");
							entry.setAttribute("lat", l.lat);
							entry.setAttribute("lon", l.lon);
							const name = gpxdoc.createElementNS(null, "name");
							const txt = gpxdoc.createTextNode(l.label);
							name.appendChild(txt);
							entry.appendChild(name);
							gpx.appendChild(entry);
						});

						gpxdoc.appendChild(gpx);

						result = '<?xml version="1.0" encoding="UTF-8"?>'+(new xmlshim.XMLSerializer()).serializeToString(gpxdoc);
						break;

					case "kml":
						const kmlns1 = "http://www.opengis.net/kml/2.2";
						const kmldoc = xmlshim.implementation.createDocument(kmlns1, "", null);
						const kml = kmldoc.createElementNS(kmlns1, "kml");
						const kmldocument = kmldoc.createElementNS(null, "Document");

						d.rows.forEach(l => {
							const place = kmldoc.createElementNS(null, "Placemark");

							const name = kmldoc.createElementNS(null, "name");
							name.appendChild(kmldoc.createTextNode(l.label));
							place.appendChild(name);

							const point = kmldoc.createElementNS(null, "Point");
							const coords = kmldoc.createElementNS(null, "coordinates");
							coords.appendChild(kmldoc.createTextNode(l.lon+','+l.lat+',0'));
							point.appendChild(coords);
							place.appendChild(point);
							kmldocument.appendChild(place);
						});

						kml.appendChild(kmldocument);
						kmldoc.appendChild(kml);

						result = '<?xml version="1.0" encoding="UTF-8"?>'+(new xmlshim.XMLSerializer()).serializeToString(kmldoc);
						break;
				}

				res.writeHead(200, {'Content-Type': 'application/force-download','Content-disposition':'attachment; filename=missing.'+req.query.format});
				res.end(result);
			}
			else {
				res.status(200).send({ "info": "No features without pictures", "details_for_humans": "This mission doesn't have any feature lacking pictures for the while (that's a good news !), you can directly review features !" });
			}
		})
		.catch(e => {
			console.error(e);
			res.status(500).send({ "error": "Failed to retrieve mission", "details_for_humans": "I can't find this mission, can you retry a bit later ?" });
		});
	}
};
