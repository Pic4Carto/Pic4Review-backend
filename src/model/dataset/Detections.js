const CONFIG = require('../../../config.json');
const Dataset = require('../Dataset');
const Feature = require('../Feature');
const GeoJSON = require('./GeoJSON');
const Hash = require('object-hash');
const P4C = require('pic4carto');

/**
 * A detections {@link Dataset} is a set of automaticly detected map features retrieved from picture providers (using Pic4Carto.js)
 * This kind of dataset is dynamic.
 *
 * @param {int} type The detection type
 * @param {Object} [options] Options for data retrieval
 * @param {LatLngBounds} [options.bbox] Bounding box for limiting search area
 */
class DetectionsDataset extends Dataset {
	constructor(type, options) {
		super();

		if(isNaN(parseInt(type)) || !P4C.Detection.TYPE_DETAILS[parseInt(type)]) {
			throw new TypeError("You should provide a valid type (use P4C.Detection.* constants)");
		}

		this.id = Hash(type);
		this.options = options || {};
		this.options.type = type;
		this.geojson = null;

		//Prepare Pic4Carto picture manager
		const picmanOpts = {};
		if(CONFIG.pictureFetchersCredentials) {
			picmanOpts.fetcherCredentials = CONFIG.pictureFetchersCredentials;
		}

		this.picman = new P4C.PicturesManager(picmanOpts);

		this._loadData();
	}

	/**
	 * Download data from Detections API
	 * @private
	 */
	_loadData() {
		this.isDownloading = true;

		this.picman
		.startDetectionsRetrieval(this.options.bbox, { types: [ this.options.type ], asgeojson: true })
		.then(data => {
			try {
				this.geojson = new GeoJSON(data);
			}
			catch(e) {
				console.error("[Detections] GeoJSON error "+e);
			}

			this.isDownloading = false;
		})
		.catch(e => {
			console.error("[Detections] Error", e);
			this.isDownloading = false;
		});
	}

	/**
	 * Override of {@link Dataset#getAllFeatures}
	 */
	getAllFeatures() {
		return new Promise((resolve, reject) => {
			if(this.isDownloading) {
				setTimeout(() => {
					resolve(this.getAllFeatures());
				}, 100);
			}
			else {
				if(this.geojson) {
					resolve(this.geojson.getAllFeatures());
				}
				else {
					reject(new Error("Features not available"));
				}
			}
		});
	}
}

module.exports = DetectionsDataset;
